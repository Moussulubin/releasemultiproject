package fr.formation.web;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.formation.ejb.IStateless;
import fr.formation.ejb.IStateful;

public class MaServlet  extends HttpServlet {

	@EJB
	IStateless less ;
	@EJB
	IStateful ful;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		int ful = this.ful.compteur();
		int less = this.less.compteur();
		
		req.setAttribute("ful", ful);
		req.setAttribute("less", less);
		
		this.getServletContext().getRequestDispatcher( "/WEB-INF/index.jsp" ).forward( req, resp );
	}
	

}
